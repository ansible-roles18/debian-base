# Debian base
Ansible role for configuring a basic debian server after cloud*init


___
## Features
- Ansible-Galaxy compatible directory structure

- Secure ssh config
    - Root managed authorized_keys
    - [Modern cipher settings](https://infosec.mozilla.org/guidelines/openssh.html#modern-openssh-67) for ssh
    - Only allow key auth, disable passwords
    - Deny root login
    - Only allow specific group to use ssh

* Install basic packages
  - debian-goodies
  - tmux
  - vim
  - htop
  - rsync
  - zstd

* Setup ansible ssh user
  - Sudo with nopasswd

- Setup postfix with gmail for status emails

* Setup ufw
  - Allow ssh port 22
  - Rate limit ssh port

- Enable unattended_upgrades
  * Reboot if needed at 05:00
  * Send emails on error

- Sends an email to {{ admin_email }} once ready


___
## Requirements
- ansible user with ssh access and sudo nopasswd permissions
___


## Ansible vars
| Name           | Required? | Default | Options | Comment                                                                                     |
|----------------|-----------|---------|---------|---------------------------------------------------------------------------------------------|
| admin_email    | YES       | none    |         | Email which receives status emails like failed updates/backups                              |
| gmail_account  | YES       | none    |         | Gmail address for sending mail                                                              |
| gmail_password | YES       | none    |         | [Gmail app password](https://support.google.com/mail/answer/185833?hl=en) for gmail_account |



